// xhr
var xmlhttp = CreateHTTPObject();

/**
 * @function: check email
 * @para: string
 * @return: boolean
 */
function isEmail( str ) {  
    var pattern = /^[-_A-Za-z0-9]+@([_A-Za-z0-9]+\.)+[A-Za-z0-9]{2,3}$/; 
    if ( pattern.test( str ) ) {
		return true; 
	}
    return false; 
}

/**
 * @function: check login password
 * @para: str
 * @return: boolean
 */
function lgPwd( str ) {
	if( str.length < 20 && str.length > 5 ) {
		return true;
	}
	return false;
}

/**
 * @function: check register password
 * @para: password object and confirm object
 * @return boolean
 */
function rgPwd( ori, conf ) {
	var chkpwd = lgPwd( ori );
	if ( ori === conf ) {
		if ( chkpwd === true ) {
			return true;
		}
	}
	return false;
}

/**
 * @function: press login button to check
 * @para: none
 * @return: boolean
 */
function login() {
	var flag = 1,
		target,
		chkname = check( "email", "uname" ),
		chkpwd = check( "lgpwd", "upwd" ),
		pwd = document.getElementById( "upwd" ),
		name = document.getElementById( "uname" ),
		atten = document.getElementById( "attention" ),
		rtstr = 0;
	if ( chkname === false ) {
		target = jAnnie.crtAni( "uname" );
		target.addFun( { typ:"bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chkpwd === false ) {
		target = jAnnie.crtAni( "upwd" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( flag === 0 ) {
		return false;
	}
	pwd.value = hex_md5( pwd.value );
	clsBtn( "submit", "正在登录..." );
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/member.php", false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		xmlhttp.send("formtype=login&uname=" + name.value + "&upwd=" + pwd.value);
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		if ( rtstr[ 'result' ] == true ) {
			setCookie( "duobei", name.value );
			window.location.href = "userlist.html";
		} else {
			atten.innerHTML = rtstr['result'];
		}
	}
	return false;
}

/**
 * @function: check form
 * @para: check type, object id
 * @return: boolean
 */
function check( type, id, conf ) {
	var obj = document.getElementById( id ),
		warn = document.getElementById( id + "warn" ),
		conf,
		flag;
	if ( conf ) {
		conf = document.getElementById( conf );
	}
	switch( type ) {
		case "email": isEmail( obj.value ) ? flag = true : flag = false; break;
		case "lgpwd": lgPwd( obj.value ) ? flag = true : flag = false; break;
		case "rgpwd": rgPwd( obj.value, conf.value ) ? flag = true : flag = false; break;
	}
	if ( flag === true ) {
		if ( conf ) {
			conf.style.borderColor = "#38b600";
		}
		obj.style.borderColor = "#38b600";
		return true;
	} else {
		if ( conf ) {
			conf.style.borderColor = "red";
		}
		obj.style.borderColor = "red";
		return false;
	}
}

/**
 * @function: check register form
 * @para: none
 * @return: boolean
 */
function register() {
	var flag = 1,
		target,
		chkname = check( "email", "uname" ),
		chkpwd = check( "lgpwd", "upwd" ),
		chknick = check( "lgpwd", "unick" ),
		chkconfirm = check( "rgpwd", "upwd", "uconfirm" ),
		name = document.getElementById( "uname" ),
		nick = document.getElementById( "unick" ),
		atten = document.getElementById( "attention" ),
		pwd = document.getElementById( "upwd" ),
		pwdconfirm = document.getElementById( "uconfirm" ),
		rtstr = 0;
	if ( chkname === false ) {
		target = jAnnie.crtAni( "uname" );
		target.addFun( { typ:"bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chkpwd === false ) {
		target = jAnnie.crtAni( "upwd" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chknick === false ) {
		target = jAnnie.crtAni( "unick" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chkconfirm === false ) {
		target = jAnnie.crtAni( "uconfirm" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( flag === 0 ) {
		return false;
	}
	pwd.value = hex_md5( pwd.value );
	pwdconfirm.value = pwd.value;
	clsBtn( "submit", "正在注册..." );
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/member.php", false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		xmlhttp.send("formtype=register&uname=" + name.value + "&unick=" + nick.value + "&upwd=" + pwd.value + "&uconfirm=" + pwdconfirm.value);
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		atten.innerHTML = rtstr['result'];
		if( atten.innerHTML !== "用户名重复" ) {
			window.location.href = "./login.html";
		}
	}
	return false;
}

/**
 * @function: update
 * @para: none
 * @return: none
 */
function update() {
	var flag = 1,
		target,
		chkpwd = check( "lgpwd", "upwd" ),
		chknick = check( "lgpwd", "unick" ),
		chkconfirm = check( "rgpwd", "upwd", "uconfirm" ),
		id = document.getElementById( "uid" ),
		nick = document.getElementById( "unick" ),
		atten = document.getElementById( "attention" ),
		pwd = document.getElementById( "upwd" ),
		pwdconfirm = document.getElementById( "uconfirm" ),
		rtstr = 0;
	if ( chkpwd === false ) {
		target = jAnnie.crtAni( "upwd" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chknick === false ) {
		target = jAnnie.crtAni( "unick" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( chkconfirm === false ) {
		target = jAnnie.crtAni( "uconfirm" );
		target.addFun( { typ: "bgc", frm: [255,255,0], to: [228,228,228], dur: 1000 });
		flag = 0;
	}
	if ( flag === 0 ) {
		return false;
	}
	pwd.value = hex_md5( pwd.value );
	pwdconfirm.value = pwd.value;
	clsBtn( "submit", "正在修改..." );
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/update.php", false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		xmlhttp.send( "id=" + id.value + "&unick=" + nick.value + "&upwd=" + pwd.value + "&uconfirm=" + pwdconfirm.value);
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		window.location.href = "userlist.html";
		atten.innerHTML = rtstr['result'];
	}
	return false;
}

/**
 * @function: disable button
 * @para: button id, button value
 * @return: none
 */
function clsBtn( obj, str ) {
	var btn = document.getElementById( obj );
	btn.value = str;
	btn.disabled = true;
}

/**
 * @function: display user list
 * @para: json, tbody id
 * @return: none
 */
function disTable( str, tbid ) {
	var tar = document.getElementById( "tbbody" ),
		i = 0,
		tr = 0,
		tdname = 0,
		tdnick = 0,
		tdpwd = 0,
		tdid = 0,
		tdtime = 0;
	for ( i = 0; i < str.length; i++ ) {
		tdidlink = document.createElement( "a" );
		tdid = document.createElement( "td" );
		tdname = document.createElement( "td" );
		tdnick = document.createElement( "td" );
		tdpwd = document.createElement( "td" );
		tddel = document.createElement( "td" );
		tdtime = document.createElement( "td" );

		tdidlink.innerHTML = str[i][0];
		tdidlink.href = "modify.html?id=" + str[i][0];
		tdname.innerHTML = str[i][1];
		tdnick.innerHTML = str[i][2];
		tdpwd.innerHTML = str[i][3];
		tddel.innerHTML = str[i][4];
		tdtime.innerHTML = str[i][5];

		tdid.style.width = "25px";
		tdname.style.width = "235px";
		tdnick.style.width = "120px";
		tdpwd.style.width = "350px";
		tdtime.style.width = "170px";

		tr = document.createElement( "tr" );
		tdid.appendChild( tdidlink );
		tr.appendChild( tdid );
		tr.appendChild( tdname );
		tr.appendChild( tdnick );
		tr.appendChild( tdpwd );
		tr.appendChild( tdtime );
		if ( tddel.innerHTML == 0 ) {
			tar.appendChild( tr );
		}
	}
}

/**
 * @function: get user list
 * @para: none
 * @return: none
 */
function getList() {
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/list.php", false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
getQueryString('test')
		xmlhttp.send();
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		disTable( rtstr, "tbbody" );
	}
}

/**
 * @function: get data of a user
 * @para: user id
 * @return: none or false
 */
function getOne( id ) {
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/user.php?id=" + id, false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		xmlhttp.send();
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		disOne( rtstr );
	}
	return false;
}

/**
 * @function: display user data
 * @para: data
 * @return: none
 */
function disOne( data ) {
	var uname = document.getElementById( "uname" ),
		unick = document.getElementById( "unick" ),
		uid = document.getElementById( "uid" );
	uid.value = data[0];
	uname.value = data[1];
	unick.value = data[2];
}

/**
 * @function: get url query string
 * @para: query
 * @return: string
 */
function getQueryString( name ) {
	if(location.href.indexOf("?")==-1 || location.href.indexOf(name+'=')==-1) {
		return '';
	}
	var queryString = location.href.substring(location.href.indexOf("?")+1);
	var parameters = queryString.split("&");
	var pos, paraName, paraValue;
	for(var i=0; i<parameters.length; i++) {
		pos = parameters[i].indexOf('=');
		if(pos == -1) { continue; }
		paraName = parameters[i].substring(0, pos);
		paraValue = parameters[i].substring(pos + 1);
		if(paraName == name) {
			return unescape(paraValue.replace(/\+/g, " "));
		}
	}
	return '';
}

/**
 * @function: setcookie
 * @para: cookie name, value and time
 * @return: none
 */
function setCookie(name, value, seconds) {
	seconds = seconds || 0;
	var expires = "";
	if (seconds != 0 ) {
		var date = new Date();
		date.setTime(date.getTime()+(seconds*1000));
		expires = "; expires="+date.toGMTString();
	}
	document.cookie = name+"="+escape(value)+expires+"; path=/";
}

/**
 * @function: clear cookie
 * @para: cookie name
 * @return: none
 */
function clearCookie(name) {
	setCookie(name, "", -1);
}

/**
 * @function: get cookie
 * @para: cookie name
 * @return: boolean
 */
function getCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') {
			c = c.substring(1,c.length);
		}
		if (c.indexOf(nameEQ) == 0) {
			return unescape(c.substring(nameEQ.length,c.length));
		}
	}
	return false;
}

/**
 * @function: check cookie
 * @para: cookie name
 * @return: none
 */
function checkCookie( name ) {
	var loginlink = document.getElementById( "loginlink" );
	if ( getCookie( name ) ) {
		loginlink.onclick = function() {
			clearCookie( name );
		};
		loginlink.innerHTML = "注销" + getCookie( name );
	} else {
		document.body.style.display = "none";
		window.location.href = "./login.html"
	}
}

/**
 * @function: delete
 * @para: id
 * @return: boolean
 */
function del() {
	var id = document.getElementById( "uid" ),
		atten = document.getElementById( "attention" );
	if (xmlhttp) {
		xmlhttp.open("POST", "./func/del.php", false);
		//xmlhttp.onreadystatechange = OnReadyStateChng;
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

		xmlhttp.send("id=" + id.value);
		rtstr = eval( '(' + xmlhttp.responseText + ')' );
		atten.innerHTML = rtstr['result'];
		if( atten.innerHTML !== "删除失败" ) {
			window.location.href = "./userlist.html";
		}
	}
}
