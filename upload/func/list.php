<?php

require_once("./core/conn.php");
require_once("./core/core.php");

header( 'Content-type: text/json' );

$restr = getlist( $conn );

if ( $restr ) {
	$json = array ();
	for ( $i = 0; $i < sizeof( $restr ); $i++ ) {
		array_push( $json, $restr[$i] );
	}
}

echo json_encode($json);

?>
