<?php

require_once("./core/conn.php");
require_once("./core/core.php");

header( 'Content-type: text/json' );

if ( $_REQUEST[ 'id' ] ) {
	$restr = del( $_REQUEST[ 'id' ], $conn );
}

if ( $restr == true ) {
	$json = array (
		"result" => $restr
	);
}

echo json_encode($json);

?>
