<?php
require_once("./core/conn.php");
require_once("./core/core.php");

header( 'Content-type: text/json' );

function getdata( $conn ) {
	if ( $_REQUEST[ 'formtype' ] ) {
		$_para = array();
		$_para[ 'uname' ] = $_REQUEST[ 'uname' ];
		$_para[ 'upwd' ] = $_REQUEST[ 'upwd' ];
		if( isset( $_REQUEST[ 'unick'] ) ) {
			$_para[ 'unick' ] = $_REQUEST[ 'unick' ];
		}
		if( isset( $_REQUEST[ 'uconfirm'] ) ) {
			$_para[ 'uconfirm' ] = $_REQUEST[ 'uconfirm' ];
		}
		switch( $_REQUEST[ 'formtype' ] ) {
			case 'login': $re = login( $_para[ 'uname' ], $_para[ 'upwd' ], $conn ); break;
			case 'register': $re = register( $_para[ 'uname' ], $_para[ 'upwd' ], $_para[ 'unick' ], $_para[ 'uconfirm' ], $conn ); break;
		}
		return $re;
	} else {
		return false;
	}
}

$restr = getdata( $conn );

if ( $restr == true ) {
	$json = array (
		"result" => $restr 
	);
}

echo json_encode($json);

?>
