<?php

require_once("./core/conn.php");
require_once("./core/core.php");

header( 'Content-type: text/json' );

if ( $_REQUEST[ 'id' ] ) {
	$restr = getone( $_REQUEST[ 'id' ], $conn );
}

if ( $restr ) {
	$json = array ();
	$json = $restr;
}

echo json_encode($json);

?>
