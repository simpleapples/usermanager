用户管理系统
==========

功能说明
==========

用户注册
用户登录
查看用户信息
删除用户信息
修改用户信息

运行环境
==========

php + mysql

安装方法
==========

将install文件夹下的install.sql用phpmyadmin导入到mysql中
修改upload/function/core/conn.php中的mysql服务器ip和端口以及用户名密码
upload文件夹为网站根目录

网站结构
==========

upload/文件夹：网站前端页面
upload/static文件夹：为js和css
upload/function文件夹：后端操作（获取用户列表、注册登录、删除、更新）接口文件
upload/function/core/core.php：后端功能实现文件
upload/function/core/conn.php：数据库配置文件
